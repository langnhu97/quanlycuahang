﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace PhanMemQuanLy.Forms.BaoCaoThongKe
{
    public partial class UCBaoCao : UserControl
    {
        string cs = System.Configuration.ConfigurationManager.ConnectionStrings["QLBHConnStr"].ToString();

        DataAccess da = new DataAccess();
        public UCBaoCao()
        {
            InitializeComponent();


        }

        private void UCBaoCao_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("proc_ChiTietHD", con);
            cmd.Parameters.Add("@MaHD", SqlDbType.VarChar, 10).Value = "HD01";
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);

            this.reportViewer1.LocalReport.ReportPath = "Report1.rdlc";
            var reportDataSource = new ReportDataSource("tbHoaDon", ds.Tables[0]);

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.LocalReport.DisplayName = "HÓA ĐƠN"; 
            this.reportViewer1.RefreshReport();
        }

        private void HienThiHD()
        {           


            //this.reportViewer1.LocalReport.ReportPath = "Report1.rdlc";
            //var reportDataSource = new ReportDataSource("tbHoaDon", listReport);        

            //this.reportViewer1.LocalReport.DataSources.Clear();
            //this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            //this.reportViewer1.RefreshReport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HienThiHD();
        }
    }
}
