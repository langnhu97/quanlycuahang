﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCThongKe : DevExpress.XtraEditors.XtraUserControl
    {
        public UCThongKe()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        private void labelControl1_Click(object sender, EventArgs e)
        {
            cbNam.DataSource = da.GetSqlTable("SELECT YEAR(NgayGiao) FROM PhieuNhap UNION SELECT YEAR(NgayBan) FROM HoaDon");
        }

        private void UCBaoCao_Load(object sender, EventArgs e)
        {
            cbNam.BindingContext = new BindingContext();
            cbNam.DataSource = da.GetSqlTable("SELECT YEAR(NgayBan) AS Nam FROM HoaDon UNION SELECT YEAR(NgayGiao) AS Nam FROM PhieuNhap");
            cbNam.ValueMember = "Nam";
            cbNam.DisplayMember = "Nam";
            //cbNam.SelectedItem = -1;
            
        }

        private void cbThang_DisplayMemberChanged(object sender, EventArgs e)
        {

        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            //Thống kê Phiếu nhập theo tháng bằng biểu đồ
            fillChartPN();
            //Thống kê Hóa đơn theo tháng bằng biểu đồ
            fillChartHD();            
        }

        public void ExportHD(DataTable dt, string sheetName, string title)
        {
            //Tạo các đối tượng Excel
            Microsoft.Office.Interop.Excel.Application oExcel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbooks oBooks;
            Microsoft.Office.Interop.Excel.Sheets oSheets;
            Microsoft.Office.Interop.Excel.Workbook oBook;
            Microsoft.Office.Interop.Excel.Worksheet oSheet;

            //Tạo mới một Excel WorkBook 
            oExcel.Visible = true;
            oExcel.DisplayAlerts = false;
            oExcel.Application.SheetsInNewWorkbook = 1;
            oBooks = oExcel.Workbooks;

            oBook = (Microsoft.Office.Interop.Excel.Workbook)(oExcel.Workbooks.Add(Type.Missing));
            oSheets = oBook.Worksheets;
            oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oSheets.get_Item(1);
            oSheet.Name = sheetName;

            // Tạo Header
            Microsoft.Office.Interop.Excel.Range head = oSheet.get_Range("A1", "J1");
            head.MergeCells = true;
            head.Value2 = title;
            head.Font.Bold = true;
            head.Font.Name = "Times New Roman";
            head.Font.Size = "18";
            head.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            // Tạo tiêu đề cột 
            Microsoft.Office.Interop.Excel.Range cl1 = oSheet.get_Range("A3", "A3");
            cl1.Value2 = "Mã HD";
            cl1.ColumnWidth = 13.5;

            Microsoft.Office.Interop.Excel.Range cl2 = oSheet.get_Range("B3", "B3");
            cl2.Value2 = "Mã KH";
            cl2.ColumnWidth = 13.5;

            Microsoft.Office.Interop.Excel.Range cl3 = oSheet.get_Range("C3", "C3");
            cl3.Value2 = "Mã NV";
            cl3.ColumnWidth = 13.5;

            Microsoft.Office.Interop.Excel.Range cl4 = oSheet.get_Range("D3", "D3");
            cl4.Value2 = "Ngày bán";
            cl4.ColumnWidth = 13.5;

            Microsoft.Office.Interop.Excel.Range cl5 = oSheet.get_Range("E3", "E3");
            cl5.Value2 = "Tổng tiền";
            cl5.ColumnWidth = 13.5;


            Microsoft.Office.Interop.Excel.Range rowHead = oSheet.get_Range("A3", "E3");
            rowHead.Font.Bold = true;


            // Tạo mảng đối tượng để lưu dữ toàn bồ dữ liệu trong DataTable,
            // vì dữ liệu được được gán vào các Cell trong Excel phải thông qua object thuần.
            object[,] arr = new object[dt.Rows.Count, dt.Columns.Count];

            //Chuyển dữ liệu từ DataTable vào mảng đối tượng
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                DataRow dr = dt.Rows[r];
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    arr[r, c] = dr[c];
                }
            }

            //Thiết lập vùng điền dữ liệu
            int rowStart = 4;
            int columnStart = 1;

            int rowEnd = rowStart + dt.Rows.Count - 1;
            int columnEnd = dt.Columns.Count;

            // Ô bắt đầu điền dữ liệu
            Microsoft.Office.Interop.Excel.Range c1 = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[rowStart, columnStart];
            // Ô kết thúc điền dữ liệu
            Microsoft.Office.Interop.Excel.Range c2 = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[rowEnd, columnEnd];
            // Lấy về vùng điền dữ liệu
            Microsoft.Office.Interop.Excel.Range range = oSheet.get_Range(c1, c2);

            //Điền dữ liệu vào vùng đã thiết lập
            range.Value2 = arr;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvHoaDon.DataSource;
            ExportHD(dt, "DANH SÁCH HÓA ĐƠN", "DANH SÁCH HÓA ĐƠN BÁN");
        }

        public void ExportPN(DataTable dt, string sheetName, string title)
        {
            //Tạo các đối tượng Excel
            Microsoft.Office.Interop.Excel.Application oExcel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbooks oBooks;
            Microsoft.Office.Interop.Excel.Sheets oSheets;
            Microsoft.Office.Interop.Excel.Workbook oBook;
            Microsoft.Office.Interop.Excel.Worksheet oSheet;

            //Tạo mới một Excel WorkBook 
            oExcel.Visible = true;
            oExcel.DisplayAlerts = false;
            oExcel.Application.SheetsInNewWorkbook = 1;
            oBooks = oExcel.Workbooks;

            oBook = (Microsoft.Office.Interop.Excel.Workbook)(oExcel.Workbooks.Add(Type.Missing));
            oSheets = oBook.Worksheets;
            oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oSheets.get_Item(1);
            oSheet.Name = sheetName;

            // Tạo phần Header
            Microsoft.Office.Interop.Excel.Range head = oSheet.get_Range("A1", "J1");
            head.MergeCells = true;
            head.Value2 = title;
            head.Font.Bold = true;
            head.Font.Name = "Times New Roman";
            head.Font.Size = "18";
            head.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            // Tạo tiêu đề cột 
            Microsoft.Office.Interop.Excel.Range cl1 = oSheet.get_Range("A3", "A3");
            cl1.Value2 = "Mã PN";
            cl1.ColumnWidth = 13.5;

            Microsoft.Office.Interop.Excel.Range cl2 = oSheet.get_Range("B3", "B3");
            cl2.Value2 = "Mã NCC";
            cl2.ColumnWidth = 13.5;

            Microsoft.Office.Interop.Excel.Range cl3 = oSheet.get_Range("C3", "C3");
            cl3.Value2 = "Ngày giao";
            cl3.ColumnWidth = 25.0;

            Microsoft.Office.Interop.Excel.Range cl4 = oSheet.get_Range("D3", "D3");
            cl4.Value2 = "Ngày bán";
            cl4.ColumnWidth = 13.5;

            Microsoft.Office.Interop.Excel.Range rowHead = oSheet.get_Range("A3", "D3");
            rowHead.Font.Bold = true;


            // Tạo mảng đối tượng để lưu dữ toàn bồ dữ liệu trong DataTable,
            // vì dữ liệu được được gán vào các Cell trong Excel phải thông qua object thuần.
            object[,] arr = new object[dt.Rows.Count, dt.Columns.Count];

            //Chuyển dữ liệu từ DataTable vào mảng đối tượng
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                DataRow dr = dt.Rows[r];
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    arr[r, c] = dr[c];
                }
            }

            //Thiết lập vùng điền dữ liệu
            int rowStart = 4;
            int columnStart = 1;

            int rowEnd = rowStart + dt.Rows.Count - 1;
            int columnEnd = dt.Columns.Count;

            // Ô bắt đầu điền dữ liệu
            Microsoft.Office.Interop.Excel.Range c1 = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[rowStart, columnStart];
            // Ô kết thúc điền dữ liệu
            Microsoft.Office.Interop.Excel.Range c2 = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[rowEnd, columnEnd];
            // Lấy về vùng điền dữ liệu
            Microsoft.Office.Interop.Excel.Range range = oSheet.get_Range(c1, c2);

            //Điền dữ liệu vào vùng đã thiết lập
            range.Value2 = arr;


        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvPhieuNhap.DataSource;
            ExportPN(dt, "DANH SÁCH PHIẾU NHẬP", "DANH SÁCH PHIẾU NHẬP HÀNG");
        }

        private void cbNam_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Windows.Forms.ComboBox cb = (System.Windows.Forms.ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }
            int nam = Convert.ToInt32(cbNam.SelectedValue.ToString().Trim());
        }

        public void fillChartPN()
        {
            int nam = Convert.ToInt32(cbNam.SelectedValue.ToString().Trim());
            string sql = $"SELECT SUM(TongTien) as 'DoanhThu', MONTH(NgayGiao) as 'Thang' FROM PhieuNhap WHERE YEAR(NgayGiao) = {nam} GROUP BY MONTH(NgayGiao)";
            DataTable dt = da.GetSqlTable(sql);
            chart1.DataSource = dt;

            chart1.Series["TongTienPN"].XValueMember = "Thang";
            chart1.Series["TongTienPN"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            chart1.Series["TongTienPN"].YValueMembers = "DoanhThu";
            chart1.Series["TongTienPN"].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
        }

        private void fillChartHD()
        {
            int nam = Convert.ToInt32(cbNam.SelectedValue.ToString().Trim());

            string sql = $"SELECT SUM(TongTien) as 'DoanhThu', MONTH(NgayBan)  as 'Thang' FROM HoaDon WHERE YEAR(NgayBan) = {nam} GROUP BY MONTH(NgayBan)";
            DataTable dt = da.GetSqlTable(sql);
            chart2.DataSource = dt;

            chart2.Series["DoanhThuHD"].XValueMember = "Thang";
            chart2.Series["DoanhThuHD"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            chart2.Series["DoanhThuHD"].YValueMembers = "DoanhThu";
            chart2.Series["DoanhThuHD"].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            int nam = int.Parse(cbNam.SelectedValue.ToString());

            DateTime startDate = dtpNgayBatDau.Value;
            DateTime endDate = dtpNgayKetThuc.Value;


            //Phiếu nhập
            //dgvPhieuNhap.DataSource = da.GetSqlTable($"SELECT * FROM PhieuNhap WHERE YEAR(NgayGiao) = {nam} AND MONTH(NgayGiao) = {thang}");
            dgvPhieuNhap.DataSource = da.GetSqlTable($"SELECT * FROM PhieuNhap WHERE NgayGiao BETWEEN '{startDate}' AND '{endDate}'");

            float tongTienPN = 0;
            int soPN = dgvPhieuNhap.RowCount;
            for (int i = 0; i < soPN - 1; i++)
            {
                tongTienPN += float.Parse(dgvPhieuNhap.Rows[i].Cells["TongTien"].Value.ToString());
            }

            txtTongTienPN.Text = tongTienPN.ToString();

            //Hóa đơn

            //dgvHoaDon.DataSource = da.GetSqlTable($"SElECT * FROM HoaDon WHERE YEAR(NgayBan) = {nam} AND MONTH(NgayBan) = {thang}");
            dgvHoaDon.DataSource = da.GetSqlTable($"SELECT MaHD, MaKH, HoaDon.MaNV, Nhanvien.HotenNV, NgayBan, TongTien" +
                $" FROM HoaDon JOIN NhanVien ON HoaDon.MaNV = Nhanvien.MaNV WHERE NgayBan   BETWEEN '{startDate}' AND '{endDate}'");
            float tongTienHD = 0;
            int soHD = dgvHoaDon.RowCount;
            for (int i = 0; i < soHD - 1; i++)
            {
                tongTienHD += float.Parse(dgvHoaDon.Rows[i].Cells["TongTien"].Value.ToString());
            }
            txtTongTienHD.Text = tongTienHD.ToString();
        }
    }
}