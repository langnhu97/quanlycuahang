﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy.Forms.BaoCaoThongKe
{
    public partial class frmHDCT : Form
    {
        public frmHDCT()
        {
            InitializeComponent();
        }

        private void frmHDCT_Load(object sender, EventArgs e)
        {
            dataSet2.EnforceConstraints = false;
            string mahd = UCHoaDon._mahd;
            this.proc_ChiTietHDTableAdapter.Fill(this.dataSet2.proc_ChiTietHD, mahd);
            this.reportViewer1.RefreshReport();
        }
    }
}
