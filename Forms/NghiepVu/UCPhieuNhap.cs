﻿using DevExpress.Utils.Extensions;
using DevExpress.XtraGrid.Views.Tile;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCPhieuNhap : UserControl
    {
        private float tongTien = 0;
        string maPN = string.Empty;
        public UCPhieuNhap()
        {
            InitializeComponent();
        }

        //private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        //{

        //}

        DataAccess da = new DataAccess();

        void HienThiPN()
        {
            dgvPhieuNhap.DataSource = da.GetSqlTable("SELECT * FROM PhieuNhap");

        }

        void HienThiDSSP()
        {
            string mapn = txtMaPN.Text.Trim();
            string mancc = cbNCC.SelectedValue.ToString().Trim();
            cbSanPham.DataSource = da.GetSqlTable($"SELECT MaSP, TenSP FROM SanPham WHERE MaNCC LIKE '{mancc}'");
            cbSanPham.DisplayMember = "TenSP";
            cbSanPham.ValueMember = "MaSP";

            dgvChiTietPN.DataSource = da.GetSqlTable($"SELECT * FROM ChiTietPN WHERE MaPN LIKE '{mapn}'");
        }
        private void UCPhieuNhap_Load(object sender, EventArgs e)
        {
            //using (QUANLYBANHANGEntities1 db = new QUANLYBANHANGEntities1())
            //{
            //    sanPhamBindingSource.DataSource = db.SanPhams.ToList();
            //}
                
            cbNCC.DataSource = da.GetSqlTable("SELECT MaNCC, TenNCC FROM NhaCungCap");
            cbNCC.DisplayMember = "TenNCC";
            cbNCC.ValueMember = "MaNCC";

            cbSanPham.DataSource = da.GetSqlTable("SELECT MaSP, TenSP FROM SanPham");
            cbSanPham.DisplayMember = "TenSP";
            cbSanPham.ValueMember = "MaSP";

            HienThiPN();
        }


        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string mapn = dgvPhieuNhap.CurrentRow.Cells[0].Value.ToString();
            string sql = $"DELETE FROM PhieuNhap WHERE MaPN LIKE '{mapn}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã xóa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa xóa được DL", "Thông báo");
                }
            }
            HienThiPN();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnXacNhan1_Click(object sender, EventArgs e)
        {
            string mapn = txtMaPN.Text.Trim();
            string mancc = cbNCC.SelectedValue.ToString().Trim();           
            DateTime ngaygiao = new DateTime(dtpNgayGiao.Value.Year, dtpNgayGiao.Value.Month, dtpNgayGiao.Value.Day);
                       

            //string sql = $"INSERT INTO PhieuNhap(MaNCC,MaSP,NgayGiao,SoLuong,DonGia,ThanhTien,MaNV)" +
            //    $"VALUES ({mancc},'{masp}','{ngaygiao}',{soluong},{donGia},{soluong}*{donGia},'{manv}')";

            string sql = $"INSERT INTO PhieuNhap(MaPN, MaNCC, NgayGiao)" +
                $" VALUES('{mapn}','{mancc}','{ngaygiao}')";

            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                MessageBox.Show("Đã thêm phiếu thành công");
                maPN = mapn;
            }
            else
            {
                MessageBox.Show("Không thành công");
            }
            HienThiPN();
            HienThiDSSP();
        }

        private void dgvPhieuNhap_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMaPN.Text = dgvPhieuNhap.CurrentRow.Cells["MaPN"].Value.ToString();
            cbNCC.SelectedValue = dgvPhieuNhap.CurrentRow.Cells["MaNCC"].Value.ToString();           
            dtpNgayGiao.Value = Convert.ToDateTime(dgvPhieuNhap.CurrentRow.Cells["NgayGiao"].Value.ToString());
            HienThiDSSP();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string mapn = dgvPhieuNhap.CurrentRow.Cells[0].Value.ToString();
            string mancc = cbNCC.SelectedValue.ToString();
            DateTime ngaygiao = new DateTime(dtpNgayGiao.Value.Year, dtpNgayGiao.Value.Month, dtpNgayGiao.Value.Day);

            string sql = $"UPDATE PhieuNhap SET MaNCC = '{mancc}', NgayGiao = '{ngaygiao}'" +
                $"WHERE MaPN LIKE '{mapn}'";

            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                MessageBox.Show("Đã sửa phiếu thành công");
            }
            else
            {
                MessageBox.Show("Không thành công");
            }
            HienThiPN();
            HienThiDSSP();
        }

        private void btnNhap_Click(object sender, EventArgs e)
        {
            string mapn = txtMaPN.Text.Trim();
            string masp = cbSanPham.SelectedValue.ToString().Trim();
            int soluong = (int.Parse(txtSoLuong.Text) > 0) ? int.Parse(txtSoLuong.Text) : 0;
            float gianhap = float.Parse(txtGiaNhap.Text) > 0 ? float.Parse(txtGiaNhap.Text) : 0f;

            string insertCTPN = $"INSERT INTO ChiTietPN (MaPN, MaSP, SoLuong, GiaNhap, ThanhTien) " +
                $"VALUES ('{mapn}', '{masp}', {soluong}, {gianhap}, {soluong}*{gianhap})";

            //string updatePNQuery = $"UPDATE PhieuNhap SET TongTien += {soluong}*{gianhap} WHERE mapn LIKE '{mapn}'";
            //string updateSPQuery = $"UPDATE SanPham SET SoLuong += {soluong}, DonGia = {gianhap} WHERE MaSP LIKE '{masp}'";
            if (da.ExecuteNonQueryCmd(insertCTPN) > 0 )
            {
                
            }
            else
            {
                MessageBox.Show("Không thành công. Kiểm tra sản phẩm đã có trong giỏ hàng");
            }

            HienThiPN();
            HienThiDSSP();
        }

        private void btnSuaSP_Click(object sender, EventArgs e)
        {
            string mapn = txtMaPN.Text.Trim();
            string masp = cbSanPham.SelectedValue.ToString().Trim();
            int soluong = int.Parse(txtSoLuong.Text) > 0 ? int.Parse(txtSoLuong.Text) : 0;
            float gianhap = float.Parse(txtGiaNhap.Text) > 0 ? float.Parse(txtGiaNhap.Text) : 0f;
            float thanhtien = Convert.ToSingle(soluong) * gianhap;

            string updateCTPNQuery = $"UPDATE ChiTietPN SET SoLuong = {soluong}, " +
                $"GiaNhap = {gianhap}, ThanhTien = {thanhtien}" +
                $" WHERE mapn LIKE '{mapn}' AND MaSP LIKE '{masp}'";

            if (da.ExecuteNonQueryCmd(updateCTPNQuery) > 0)
            {
                MessageBox.Show("Đã thay đổi");                
            }
            HienThiPN();
            HienThiDSSP();
        }

        private void btnXoaSP_Click(object sender, EventArgs e)
        {
            string mapn = dgvChiTietPN.CurrentRow.Cells[0].Value.ToString();
            string masp = dgvChiTietPN.CurrentRow.Cells[1].Value.ToString();
            float thanhtien = float.Parse(dgvChiTietPN.CurrentRow.Cells["ThanhTien"].Value.ToString());
            string deleteCTHDQuery = $"DELETE FROM ChiTietPN WHERE mapn LIKE '{mapn}' AND MaSP LIKE '{masp}'";
            DialogResult dlr = MessageBox.Show("Bạn thực sự muốn bỏ sản phẩm này?", "Thông báo", MessageBoxButtons.YesNo);

            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(deleteCTHDQuery) > 0)
                {
                    
                }
                else
                {
                    MessageBox.Show("Xóa không thành công");
                }
            }
            HienThiPN();
            HienThiDSSP();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            string ma = txtMaPN.Text.Trim();
            string masp = cbSanPham.SelectedValue.ToString().Trim();
            string sql1 = $"DELETE FROM PhieuNhap WHERE MaPN LIKE '{ma}'";
            string sql2 = $"DELETE FROM ChiTietPN WHERE MaPN LIKE '{ma}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql2) >= 0 && da.ExecuteNonQueryCmd(sql1) > 0)
                {
                    MessageBox.Show("Đã xóa PN thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa xóa được PN", "Thông báo");
                }
            }
            HienThiPN();
            HienThiDSSP();
        }

        private void dgvChiTietPN_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            cbSanPham.SelectedValue = dgvChiTietPN.CurrentRow.Cells[1].Value.ToString().Trim();
            txtSoLuong.Text = dgvChiTietPN.CurrentRow.Cells[2].Value.ToString().Trim();
            txtGiaNhap.Text = dgvChiTietPN.CurrentRow.Cells[3].Value.ToString().Trim();
        }

        private void cbNCC_TextChanged(object sender, EventArgs e)
        {
        }

        private void dgvPhieuNhap_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvPhieuNhap_SelectionChanged(object sender, EventArgs e)
        {
            dgvPhieuNhap.CurrentRow.DefaultCellStyle.SelectionBackColor = Color.Red;
        }

        private void btnLuuPN_Click(object sender, EventArgs e)
        {
            float sum = 0;
            string maPN = dgvPhieuNhap.CurrentRow.Cells[0].Value.ToString().Trim();
            if(MessageBox.Show("Bạn chắc chắn thanh toán", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    foreach (DataGridViewRow dgvr in dgvChiTietPN.Rows)
                    {
                        if (dgvr.Cells[0].Value == null) break;
                        sum += float.Parse(dgvr.Cells[4].Value.ToString().Trim());

                        string masp = dgvr.Cells[1].Value.ToString().Trim();
                        int soluong = Convert.ToInt32(dgvr.Cells[2].Value.ToString().Trim());
                        float giaNhap = Convert.ToSingle(dgvr.Cells[3].Value.ToString().Trim());
                        string sql1 = $"UPDATE SanPham SET SoLuong += {soluong}, GiaNhap = {giaNhap} WHERE MaSP = '{masp}'";
                        da.ExecuteNonQueryCmd(sql1);
                    }

                    txtThanhToan.Text = sum.ToString();
                    string sql = $"UPDATE PhieuNhap SET TongTien = {sum} WHERE MaPN = '{maPN}'";

                    if (da.ExecuteNonQueryCmd(sql) > 0)
                    {
                        MessageBox.Show("Thanh toán phiếu nhập thành công!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Chưa thanh toán được phiếu nhập!");
                }
            }            

            HienThiPN();
            HienThiDSSP();
        }

        private void dgvChiTietPN_SelectionChanged(object sender, EventArgs e)
        {
            dgvChiTietPN.CurrentRow.DefaultCellStyle.SelectionBackColor = Color.Green;
        }

        private void cbSanPham_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["QLBHConnStr"].ToString());
            try
            {
                sqlCon.Open();
                string masp = cbSanPham.SelectedValue.ToString().Trim();
                string sql = $"SELECT * FROM SanPham WHERE MaSP = '{masp}'";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                double gianhap = 0;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            gianhap = reader.GetDouble(4);
                        }
                    }
                }
                txtGiaNhap.Text = gianhap.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        private void dgvChiTietPN_CellValidated(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
