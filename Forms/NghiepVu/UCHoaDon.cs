﻿using DevExpress.Office.Drawing;
using DevExpress.Skins;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler.Design;
using PhanMemQuanLy.Forms.BaoCaoThongKe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCHoaDon : DevExpress.XtraEditors.XtraUserControl
    {
        public UCHoaDon()
        {
            InitializeComponent();
        }
        
        DataAccess da = new DataAccess();
        public static string _mahd;

        void HienThiHD()
        {
            dgvHoaDon.DataSource = da.GetSqlTable("SELECT * FROM HoaDon");
        }

        void HienThiChiTietHD()
        {
            string mahd = txtMaHD.Text.Trim();;
            dgvChiTietHD.DataSource = da.GetSqlTable($"SElECT * FROM ChiTietHD WHERE MaHD LIKE '{mahd}'");
        }
        private void UCHoaDon_Load(object sender, EventArgs e)
        {
            cbMaKH.DataSource = da.GetSqlTable("SELECT MaKH, TenKH FROM KhachHang");
            cbMaKH.DisplayMember = "TenKH";
            cbMaKH.ValueMember = "MaKH";

            cbMaNV.DataSource = da.GetSqlTable("SELECT MaNV, HoTenNV FROM NhanVien");
            cbMaNV.DisplayMember = "HoTenNV";
            cbMaNV.ValueMember = "MaNV";

            cbSanPham.DataSource = da.GetSqlTable("SELECT MaSP, TenSP FROM SanPham");
            cbSanPham.DisplayMember = "TenSP";
            cbSanPham.ValueMember = "MaSP";

            HienThiHD();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string mahd = dgvHoaDon.CurrentRow.Cells[0].Value.ToString();
            string sql = $"DELETE FROM HoaDon WHERE MaHD = '{mahd}'";
            string sql2 = $"DELETE FROM ChiTietHD WHERE MaHD = '{mahd}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn xóa HĐ này?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql2) >= 0 && da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã xóa HĐ thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa xóa được HĐ", "Thông báo");
                }
            }
            HienThiHD();
            HienThiChiTietHD();
        }

        private void dgvHoaDon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMaHD.Text = dgvHoaDon.CurrentRow.Cells["MaHD"].Value.ToString();
            cbMaKH.SelectedValue = dgvHoaDon.CurrentRow.Cells["MaKH"].Value.ToString();
            cbMaNV.SelectedValue = dgvHoaDon.CurrentRow.Cells["MaNV"].Value.ToString();
            dtpNgayGiao.Value = Convert.ToDateTime(dgvHoaDon.CurrentRow.Cells["NgayBan"].Value.ToString());
            dgvHoaDon.CurrentRow.DefaultCellStyle.SelectionBackColor = Color.Red;
            HienThiChiTietHD();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }
        

        private void btnXacNhan1_Click(object sender, EventArgs e)
        {
            string mahd = txtMaHD.Text.Trim();;
            int makh = Convert.ToInt32(cbMaKH.SelectedValue.ToString());
            string manv = cbMaNV.SelectedValue.ToString();
            DateTime ngayban = new DateTime(dtpNgayGiao.Value.Year, dtpNgayGiao.Value.Month, dtpNgayGiao.Value.Day);


            string sql = $"INSERT INTO HoaDon(MaHD,MaKH,MaNV,NgayBan)" +
                $"VALUES ('{mahd}', {makh},'{manv}','{ngayban}')";
            

            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                MessageBox.Show("Đã thêm Hóa đơn thành công");
            }
            else
            {
                MessageBox.Show("KHÔNG thành công.");
            }
            HienThiHD();
        }

        private void btnChonMua_Click(object sender, EventArgs e)
        {
            string mahd = txtMaHD.Text.Trim();;
            string masp = cbSanPham.SelectedValue.ToString();
            int soluong = (int.Parse(txtSoLuong.Text) > 0) ? int.Parse(txtSoLuong.Text) : 0;
            float giaban = float.Parse(txtGiaBan.Text) > 0 ? float.Parse(txtGiaBan.Text) : 0f;

            string insertCTHD = $"INSERT INTO ChiTietHD (MaHD, MaSP, SoLuong, GiaBan, ThanhTien) " +
                $"VALUES ('{mahd}', '{masp}', {soluong}, {giaban}, {soluong}*{giaban})";

            //string updateHDQuery = $"UPDATE HoaDon SET TongTien += {soluong}*{giaban} WHERE MaHD LIKE '{mahd}'";
            //string updateSPQuery = $"UPDATE SanPham SET SoLuong -= {soluong} WHERE MaSP LIKE '{masp}'";
            if (da.ExecuteNonQueryCmd(insertCTHD) > 0)
            {
                
            }
            else
            {
                MessageBox.Show("Không thành công. Sản phẩm đã có trong giỏ hàng");
            }

            HienThiHD();
            dgvChiTietHD.DataSource = da.GetSqlTable($"SELECT * FROM ChiTietHD WHERE MaHD LIKE '{mahd}'");
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string mahd = dgvHoaDon.CurrentRow.Cells[0].Value.ToString();
            int makh = Convert.ToInt32(cbMaKH.SelectedValue.ToString());
            string manv = cbMaNV.SelectedValue.ToString();

            DateTime ngayban = new DateTime(dtpNgayGiao.Value.Year, dtpNgayGiao.Value.Month, dtpNgayGiao.Value.Day);
            string sql = $"UPDATE HoaDon SET MaKH = '{makh}', MaNV = '{manv}', NgayBan = '{ngayban}' WHERE MaHD = '{mahd}'";

            if(da.ExecuteNonQueryCmd(sql) > 0)
            {
                MessageBox.Show("Thay đổi DL thành công!");
                HienThiHD();
            }
            else
            {
                MessageBox.Show("Không thành công!");
            }
        }

        private void dgvChiTietHD_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string masp = dgvChiTietHD.CurrentRow.Cells["MaSP"].Value.ToString();
                cbSanPham.SelectedValue = masp;
                txtTonKho.Text = da.ExecuteScalar($"SELECT SoLuong FROM SanPham WHERE Masp = '{masp}'");
                txtSoLuong.Text = dgvChiTietHD.CurrentRow.Cells["SoLuong"].Value.ToString();
                txtGiaBan.Text = dgvChiTietHD.CurrentRow.Cells["GiaBan"].Value.ToString();
                dgvChiTietHD.CurrentRow.DefaultCellStyle.BackColor = Color.DarkCyan;
            }
            catch (Exception)
            {
                MessageBox.Show("Chọn chưa đúng");
            }
        }

        private void btnSuaSP_Click(object sender, EventArgs e)
        {
            string mahd = txtMaHD.Text.Trim();;
            string masp = cbSanPham.SelectedValue.ToString();
            int soluong = int.Parse(txtSoLuong.Text) > 0 ? int.Parse(txtSoLuong.Text) : 0;
            float GiaBan = float.Parse(txtGiaBan.Text) > 0 ? float.Parse(txtGiaBan.Text) : 0f;

            float thanhtiencu = int.Parse(dgvChiTietHD.CurrentRow.Cells["ThanhTien"].Value.ToString());
            
            string updateCTHDQuery = $"UPDATE ChiTietHD SET SoLuong = {soluong}, " +
                $"GiaBan = {GiaBan}, ThanhTien = {soluong} * {GiaBan}" +
                $" WHERE MaHD LIKE '{mahd}' AND MaSP LIKE '{masp}'";


            if(da.ExecuteNonQueryCmd(updateCTHDQuery) > 0)
            {
                MessageBox.Show("Đã thay đổi");
            }
            HienThiHD();
            HienThiChiTietHD();
        }

        private void btnXoaSP_Click(object sender, EventArgs e)
        {
            string mahd = txtMaHD.Text.Trim();;
            string masp = cbSanPham.SelectedValue.ToString();
            float thanhtien = float.Parse(dgvChiTietHD.CurrentRow.Cells["ThanhTien"].Value.ToString());
            string deleteCTHDQuery = $"DELETE FROM ChiTietHD WHERE MaHD LIKE '{mahd}' AND MaSP LIKE '{masp}'";
            DialogResult dlr = MessageBox.Show("Bạn thực sự muốn xóa?", "Thông báo", MessageBoxButtons.YesNo);

            if(dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(deleteCTHDQuery) > 0)
                {
                    MessageBox.Show("Xóa thành công");
                }
                else
                {
                    MessageBox.Show("Xóa không thành công");
                }
            }
            HienThiHD();
            HienThiChiTietHD();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            float sum = 0;
            string maHD = dgvChiTietHD.CurrentRow.Cells[0].Value.ToString().Trim();
            try
            {
                foreach (DataGridViewRow dgvr in dgvChiTietHD.Rows)
                {
                    if (dgvr.Cells[0].Value == null) break;
                    sum += Convert.ToSingle(dgvr.Cells[4].Value.ToString().Trim());
                    string masp = dgvr.Cells[1].Value.ToString().Trim();
                    int soluong = Convert.ToInt32(dgvr.Cells[2].Value.ToString().Trim());
                    float giaban = Convert.ToSingle(dgvr.Cells[3].Value.ToString().Trim());
                    if (Convert.ToInt32(txtTonKho.Text) < soluong)
                    {
                        MessageBox.Show($"Số lượng sản phẩm mã '{masp}'trong kho không đủ");
                        return;
                    }
                    string sql1 = $"UPDATE SanPham SET SoLuong -= {soluong} WHERE MaSP = '{masp}'";
                    da.ExecuteNonQueryCmd(sql1);
                }                

                txtThanhToan.Text = sum.ToString();
                string sql = $"UPDATE HoaDon SET TongTien = {sum} WHERE MaHD = '{maHD}'";
                
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Thanh toán hóa đơn thành công!");
                    HienThiHD();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Chưa thanh toán được hóa đơn!");
                return;
            }
            MessageBox.Show("Thanh toán hóa đơn thành công!");
        }

        private void btnHuyHD_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Bạn chắc chắn muốn hủy bỏ các sản phẩm đã chọn?", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.OK)
            {
                string mahd = dgvHoaDon.CurrentRow.Cells[0].Value.ToString();
                string sql = $"DELETE FROM ChiTietHD WHERE MaHD = '{mahd}'";
                if(da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã hủy bỏ! ");
                }
                else
                {
                    MessageBox.Show("Có lỗi. Kiểm tra lại!");

                }
            }
        }

        private void dgvChiTietHD_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            //int soluong = int.Parse(dgvChiTietHD.CurrentRow.Cells[2].Value.ToString());
            //float giaban = float.Parse(dgvChiTietHD.CurrentRow.Cells[3].Value.ToString());

            //var thanhtien = soluong * giaban;
            //string updateCTHDQuery = $"UPDATE HoaDon SET TongTien += {soluong}*{giaban} WHERE MaHD LIKE{txtMaHD.Text.Trim();}";

            //try
            //{
            //    int n = da.ExecuteNonQueryCmd(updateCTHDQuery);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //HienThiHD();
            
        }

        private void cbSanPham_SelectedIndexChanged(object sender, EventArgs e)
        {

            SqlConnection sqlCon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["QLBHConnStr"].ToString());
            try
            {
                sqlCon.Open();
                string masp = cbSanPham.SelectedValue.ToString();
                string sql = $"SELECT * FROM SanPham WHERE MaSP = '{masp}'";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                int slton = 0;
                double giaban = 0;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            slton = reader.GetInt32(3);
                            giaban = reader.GetDouble(6);
                        }
                    }
                }
                txtTonKho.Text = slton.ToString();
                txtGiaBan.Text = giaban.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            _mahd = txtMaHD.Text.Trim();
            frmHDCT frm = new frmHDCT();
            frm.ShowDialog();
        }
    }
}