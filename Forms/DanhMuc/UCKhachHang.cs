﻿using DevExpress.Utils;
using DevExpress.Utils.About;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCKhachHang : DevExpress.XtraEditors.XtraUserControl
    {
        public UCKhachHang()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        void HienThiDuLieuKH()
        {
            dgvKhachHang.DataSource = da.GetSqlTable("SELECT * FROM KHACHHANG");
        }

        private void UCKhachHang_Load(object sender, EventArgs e)
        {
            HienThiDuLieuKH();
        }

        private void txtTimKiem_TextChanged_1(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                string name = txtTimKiem.Text;
                string sql = $"SELECT * FROM KHACHHANG WHERE TenKH LIKE N'%{name}%'";
                dgvKhachHang.DataSource = da.GetSqlTable(sql);
            }
            else
            {
                HienThiDuLieuKH();
            }
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            string tenkh = txtTenKH.Text;
            string diachi = txtDiaChi.Text;
            string sdt = txtSDT.Text;
            string nganhang = txtNganHang.Text;
            string stk = txtSTK.Text;
            string ghichu = txtGhiChu.Text;

            string sql = $"INSERT INTO KhachHang(TenKH,DiaChi,SoDienThoai,NganHang,SoTaiKhoan,GhiChu)" +
                $"VALUES (N'{tenkh}',N'{diachi}','{sdt}',N'{nganhang}','{stk}',N'{ghichu}')";
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã thêm DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa thêm được DL", "Thông báo");
                }
            

            HienThiDuLieuKH();
        }

        private void btnSearch_Click_1(object sender, EventArgs e)
        {
            string name = txtTimKiem.Text;
            string sql = $"SELECT * FROM KHACHHANG WHERE TenKH LIKE N'%{name}%'";
            da.ExecuteNonQueryCmd(sql);
            MessageBox.Show("OK", "Thông báo");
            HienThiDuLieuKH();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            var id = int.Parse(dgvKhachHang.SelectedRows[0].Cells[0].Value.ToString());
            string sql = $"DELETE FROM KHACHHANG WHERE MaKH = {id}";

            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?","Thông báo",
                MessageBoxButtons.YesNo);

            if(dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show($"Đã xóa thành công bản ghi", "Thông báo");
                }
                else
                {
                    Console.WriteLine("Xóa không thành công", "Thông báo");
                }
            }
            
            HienThiDuLieuKH();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }

        private void dgvKhachHang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMaKH.Text = dgvKhachHang.CurrentRow.Cells["MaKH"].Value.ToString();
            txtTenKH.Text = dgvKhachHang.CurrentRow.Cells[1].Value.ToString();
            txtDiaChi.Text = dgvKhachHang.CurrentRow.Cells[2].Value.ToString();
            txtSDT.Text = dgvKhachHang.CurrentRow.Cells[3].Value.ToString();
            txtNganHang.Text = dgvKhachHang.CurrentRow.Cells[4].Value.ToString();
            txtSTK.Text = dgvKhachHang.CurrentRow.Cells[5].Value.ToString();
            txtGhiChu.Text = dgvKhachHang.CurrentRow.Cells[6].Value.ToString();

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int makh = int.Parse(txtMaKH.Text);
            string tenkh = txtTenKH.Text;
            string diachi = txtDiaChi.Text;
            string sdt = txtSDT.Text;
            string nganhang = txtNganHang.Text;
            string stk = txtSTK.Text;
            string ghichu = txtGhiChu.Text;

            string sql = $"UPDATE KhachHang SET TenKH=N'{tenkh}', DiaChi=N'{diachi}', SoDienThoai='{sdt}'," +
                $"NganHang=N'{nganhang}', SoTaiKhoan={stk}, GhiChu=N'{ghichu}' WHERE MaKH = {makh}";

            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDuLieuKH();
        }
    }
}
