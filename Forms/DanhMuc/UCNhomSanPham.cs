﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Mask.Design;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCNhomSanPham : DevExpress.XtraEditors.XtraUserControl
    {
        public UCNhomSanPham()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        void HienThiDL()
        {
            dgvNhomSP.DataSource = da.GetSqlTable("SELECT * FROM NhomSanPham");
        }

        private void UCNhomSanPham_Load(object sender, EventArgs e)
        {            
            HienThiDL();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string manhom = txtMaNhom.Text;
            string tennhom = txtTenNhom.Text;
            string ghichu = txtGhiChu.Text;

            string sql = $"INSERT INTO NhomSanPham([MaNhom],[TenNhom],[GhiChu]) VALUES" +
                $"('{manhom}',N'{tennhom}',N'{ghichu}')";


            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                Console.WriteLine("Thêm thông tin sản phẩm thành công", "Thông báo");
            }
            else
            {
                Console.WriteLine("KHÔNG thêm được thông tin sản phẩm", "Thông báo");
            }
            HienThiDL();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string manhom = txtMaNhom.Text;
            string tennhom = txtTenNhom.Text;
            string ghichu = txtGhiChu.Text;

            string sql = $"UPDATE NhomSanPham SET MaNhom='{manhom}', TenNhom=N'{tennhom}', GhiChu=N'{ghichu}'" +
                $" WHERE MaNhom LIKE '{manhom}'";

            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string ma = dgvNhomSP.SelectedRows[0].Cells[0].Value.ToString();
            string sql = $"DELETE FROM NhomSanPham WHERE MaNhom LIKE '{ma}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }

        private void dgvNhomSP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtMaNhom.Text = dgvNhomSP.CurrentRow.Cells["MaNhom"].Value.ToString();
                txtTenNhom.Text = dgvNhomSP.CurrentRow.Cells["TenNhom"].Value.ToString();
                txtGhiChu.Text = dgvNhomSP.CurrentRow.Cells["GhiChu"].Value.ToString();

            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                string tentimkiem = txtTimKiem.Text;
                string sql = $"SELECT * FROM NhomSanPham WHERE TenNhom LIKE N'%{tentimkiem}%'";
                dgvNhomSP.DataSource = da.GetSqlTable(sql);
            }
            else
            {
                HienThiDL();
            }
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
