﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using System.Security.Cryptography;

namespace PhanMemQuanLy
{
    public partial class UCNguoiDung : DevExpress.XtraEditors.XtraUserControl
    {
        public UCNguoiDung()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        void HienThiDL()
        {
            cbChucVu.DataSource = da.GetSqlTable("SELECT MaChucVu, TenChucVu FROM LoaiChucVu");
            cbChucVu.DisplayMember = "TenChucVu";
            cbChucVu.ValueMember = "MaChucVu";
            dgvNguoiDung.DataSource = da.GetSqlTable("SELECT * FROM NguoiDung ORDER BY MaChucVu ASC");
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if(txtTimKiem.Text != "")
            {
                string tentaikhoan = txtTimKiem.Text;
                string sql = $"SELECT * FROM NguoiDung WHERE TaiKhoan LIKE '%{tentaikhoan}%'";
                dgvNguoiDung.DataSource = da.GetSqlTable(sql);
            }
            else
            {
                HienThiDL();
            }
        }

        private void UCNguoiDung_Load(object sender, EventArgs e)
        {
            HienThiDL();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string taikhoan = txtTaiKhoan.Text;
            string matkhau = txtMatKhau.Text;
            int machucvu = int.Parse(cbChucVu.SelectedValue.ToString());

            string mk = da.GetMD5Hash(txtMatKhau.Text);
            string sql = $"INSERT INTO NguoiDung(TaiKhoan, MatKhau, MaChucVu)" +
                $"VALUES ('{taikhoan}',N'{mk}',N'{machucvu}')";

            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                
                MessageBox.Show("Đã thêm DL thành công", "Thông báo");
            }
            else
            {
                MessageBox.Show("KHÔNG thành công", "Thông báo");
            }
            HienThiDL();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string tentk = dgvNguoiDung.SelectedRows[0].Cells[0].Value.ToString();
            string sql = $"DELETE FROM NguoiDung WHERE TaiKhoan LIKE '{tentk}'";
            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                MessageBox.Show("Đã xóa thành công DL", "Thông báo");
            }
            else
            {
                MessageBox.Show("KHÔNG thành công", "Thông báo");
            }
            HienThiDL();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }
    }
}
