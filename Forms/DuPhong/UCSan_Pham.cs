﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Mask.Design;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace PhanMemQuanLy
{
    public partial class UCSan_Pham : DevExpress.XtraEditors.XtraUserControl
    {
        public static string filePath = string.Empty;
        public UCSan_Pham()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        void HienThiDL()
        {
            dgvSanPham.DataSource = da.GetSqlTable("SELECT * FROM SanPham");
        }

        private void UCSanPham_Load(object sender, EventArgs e)
        {
            cbMaNhom.DataSource = da.GetSqlTable("SELECT MaNhom, TenNhom FROM NhomSanPham");
            cbMaNhom.ValueMember = "MaNhom";
            cbMaNhom.DisplayMember = "TenNhom";

            cbMaNCC.DataSource = da.GetSqlTable("SELECT MaNCC, TenNCC FROM NhaCungCap");
            cbMaNCC.ValueMember = "MaNCC";
            cbMaNCC.DisplayMember = "TenNCC";
            HienThiDL();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string masp = txtMaSp.Text;
            string tensp = txtTenSp.Text;
            string manhom = cbMaNhom.SelectedValue.ToString().Trim();
            int soluong = (txtSoLuong.Text != "") ? Convert.ToInt32(txtSoLuong.Text) : 0;
            float gianhap = (txtGiaNhap.Text != "") ? Convert.ToSingle(txtGiaNhap.Text) : 0f;
            string mancc = cbMaNCC.SelectedValue.ToString().Trim();
            float giaban = (txtGiaBan.Text != "") ? Convert.ToSingle(txtGiaBan.Text) : 0f;
            

            string sql = $"INSERT INTO SanPham([MaSp],[TenSp],[MaNhom],[SoLuong],[GiaNhap],[MaNCC], [GiaBan]) VALUES" +
                $"('{masp}',N'{tensp}','{manhom}',{soluong},{gianhap},'{mancc}', {giaban})";


            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                Console.WriteLine("Thêm thông tin sản phẩm thành công", "Thông báo");
            }
            else
            {
                Console.WriteLine("KHÔNG thêm được thông tin sản phẩm", "Thông báo");
            }
            HienThiDL();
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                string tentimkiem = txtTimKiem.Text;
                string sql = $"SELECT * FROM SanPham WHERE TenSp LIKE N'%{tentimkiem}%'";
                dgvSanPham.DataSource = da.GetSqlTable(sql);
            }
            else
            {
                HienThiDL();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string ma = dgvSanPham.SelectedRows[0].Cells[0].Value.ToString();
            string sql = $"DELETE FROM SanPham WHERE MaSP LIKE '{ma}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        //private void btnCancel_Click(object sender, EventArgs e)
        //{
        //    splitContainer1.Panel1.Controls.Clear();
        //    splitContainer1.Panel2.Controls.Clear();
        //}

        //private void dgvSanPham_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    try
        //    {
        //        txtMaSp.Text = dgvSanPham.CurrentRow.Cells["MaSp"].Value.ToString();
        //        txtTenSp.Text = dgvSanPham.CurrentRow.Cells["TenSp"].Value.ToString();
        //        cbMaNhom.ValueMember = dgvSanPham.CurrentRow.Cells["MaNhom"].Value.ToString();
        //        txtSoLuong.Text = dgvSanPham.CurrentRow.Cells["SoLuong"].Value.ToString();
        //        txtGiaNhap.Text = dgvSanPham.CurrentRow.Cells["GiaNhap"].Value.ToString();
        //        cbMaNCC.ValueMember = dgvSanPham.CurrentRow.Cells["MaNCC"].Value.ToString();
        //        txtGiaBan.Text = dgvSanPham.CurrentRow.Cells["GiaBan"].Value.ToString();

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //}

        //private void btnEdit_Click(object sender, EventArgs e)
        //{
        //    string masp = txtMaSp.Text;
        //    string tensp = txtTenSp.Text;
        //    string manhom = cbMaNhom.SelectedValue.ToString();
        //    int soluong = (txtSoLuong.Text != "") ? Convert.ToInt32(txtSoLuong.Text) : 0;
        //    float gianhap = (txtGiaNhap.Text != "") ? Convert.ToSingle(txtGiaNhap.Text) : 0f;
        //    string mancc = cbMaNCC.SelectedValue.ToString();
        //    float giaban = (txtGiaBan.Text != "") ? Convert.ToSingle(txtGiaBan.Text) : 0f;


        //    string sql = $"UPDATE SanPham SET MaSP='{masp}', TenSp=N'{tensp}', MaNhom=N'{manhom}'," +
        //        $"SoLuong={soluong}, GiaNhap={gianhap}, MaNCC='{mancc}', GiaBan={giaban} WHERE MaSP LIKE '{masp}'";

        //    DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
        //       MessageBoxButtons.YesNo);
        //    if (dlr == DialogResult.Yes)
        //    {
        //        if (da.ExecuteNonQueryCmd(sql) > 0)
        //        {
        //            MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
        //        }
        //        else
        //        {
        //            MessageBox.Show("Chưa sửa được DL", "Thông báo");
        //        }
        //    }
        //    HienThiDL();
        //}

        private void dgvSanPham_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void labelControl10_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }        

        private void btnThem_Click(object sender, EventArgs e)
        {
            string masp = txtMaSp.Text;
            string tensp = txtTenSp.Text;
            string manhom = cbMaNhom.SelectedValue.ToString().Trim();
            int soluong = (txtSoLuong.Text != "") ? Convert.ToInt32(txtSoLuong.Text) : 0;
            float gianhap = (txtGiaNhap.Text != "") ? Convert.ToSingle(txtGiaNhap.Text) : 0f;
            string mancc = cbMaNCC.SelectedValue.ToString().Trim();
            float giaban = (txtGiaBan.Text != "") ? Convert.ToSingle(txtGiaBan.Text) : 0f;


            string sql = $"INSERT INTO SanPham([MaSp],[TenSp],[MaNhom],[SoLuong],[GiaNhap],[MaNCC], [GiaBan], [HinhAnh]) VALUES" +
                $"('{masp}',N'{tensp}','{manhom}',{soluong},{gianhap},'{mancc}', {giaban})";


            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                Console.WriteLine("Thêm thông tin sản phẩm thành công", "Thông báo");
            }
            else
            {
                Console.WriteLine("KHÔNG thêm được thông tin sản phẩm", "Thông báo");
            }
            HienThiDL();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string masp = txtMaSp.Text;
            string tensp = txtTenSp.Text;
            string manhom = cbMaNhom.SelectedValue.ToString();
            int soluong = (txtSoLuong.Text != "") ? Convert.ToInt32(txtSoLuong.Text) : 0;
            float gianhap = (txtGiaNhap.Text != "") ? Convert.ToSingle(txtGiaNhap.Text) : 0f;
            string mancc = cbMaNCC.SelectedValue.ToString();
            float giaban = (txtGiaBan.Text != "") ? Convert.ToSingle(txtGiaBan.Text) : 0f;


            string sql = $"UPDATE SanPham SET MaSP='{masp}', TenSp=N'{tensp}', MaNhom=N'{manhom}'," +
                $"SoLuong={soluong}, GiaNhap={gianhap}, MaNCC='{mancc}', GiaBan={giaban} WHERE MaSP LIKE '{masp}'";

            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string ma = dgvSanPham.SelectedRows[0].Cells[0].Value.ToString();
            string sql = $"DELETE FROM SanPham WHERE MaSP LIKE '{ma}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }

        private void dgvSanPham_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtMaSp.Text = dgvSanPham.CurrentRow.Cells["MaSP"].Value.ToString();
                txtTenSp.Text = dgvSanPham.CurrentRow.Cells["TenSp"].Value.ToString();
                cbMaNhom.SelectedValue = dgvSanPham.CurrentRow.Cells["MaNhom"].Value.ToString();
                txtSoLuong.Text = dgvSanPham.CurrentRow.Cells["SoLuong"].Value.ToString();
                txtGiaNhap.Text = dgvSanPham.CurrentRow.Cells["GiaNhap"].Value.ToString();
                cbMaNCC.SelectedValue = dgvSanPham.CurrentRow.Cells["MaNCC"].Value.ToString();
                txtGiaBan.Text = dgvSanPham.CurrentRow.Cells["GiaBan"].Value.ToString();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}