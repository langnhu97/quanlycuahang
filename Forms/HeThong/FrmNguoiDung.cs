﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class FrmNguoiDung : Form
    {
        DataAccess da = new DataAccess();
        public FrmNguoiDung()
        {
            InitializeComponent();
        }

        private void FrmNguoiDung_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qUANLYBANHANGDataSet.LoaiChucVu' table. You can move, or remove it, as needed.
            this.loaiChucVuTableAdapter.Fill(this.qUANLYBANHANGDataSet.LoaiChucVu);
            // TODO: This line of code loads data into the 'qUANLYBANHANGDataSet.NguoiDung' table. You can move, or remove it, as needed.
            this.nguoiDungTableAdapter.Fill(this.qUANLYBANHANGDataSet.NguoiDung);
            //cbChucVu.DataSource = da.GetSqlTable("SELECT MaChucVu, TenChucVu FROM LoaiChucVu");
            //cbChucVu.DisplayMember = "TenChucVu";
            //cbChucVu.ValueMember = "MaChucVu";
        }

        private void nguoiDungBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.nguoiDungBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.qUANLYBANHANGDataSet);

        } 

         
        private void btnLuu_Click(object sender, EventArgs e)
        {
            DataRow dr = qUANLYBANHANGDataSet.NguoiDung.NewRow();
            dr[0] = txtTaiKhoan.Text;
            string mk = da.GetMD5Hash(txtMatKhau.Text);
            dr[1] = mk;
            dr[2] = int.Parse(cbChucVu.SelectedValue.ToString());

            this.qUANLYBANHANGDataSet.NguoiDung.Rows.Add(dr);
            this.nguoiDungBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.qUANLYBANHANGDataSet);
        }

        private void btnSua_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.nguoiDungBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.qUANLYBANHANGDataSet);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Bạn có muốn xóa User này k?", "Chú ý",
               MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (dlr == DialogResult.Yes)
            {
                this.nguoiDungBindingSource.RemoveCurrent();
                this.Validate();
                this.nguoiDungBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.qUANLYBANHANGDataSet);
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Bạn có muốn thoát?", "Chú ý",
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (dlr == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
