﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class FrmDoiMatKhau : Form
    {
        DataAccess da = new DataAccess();
       
        public FrmDoiMatKhau()
        {
            InitializeComponent();
        }

        private void txtTaiKhoan_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            string username = LoginInfo.TKLogin;
            string password = LoginInfo.MKLogin;
            string mkCu = txtMatKhauCu.Text;
            string mkMoi = txtMatKhauMoi.Text;
            string mkXacNhan = txtXacNhan.Text;

            if (mkCu.Equals(password) && mkXacNhan.Equals(mkMoi))
            {
                string sql = $"UPDATE NguoiDung SET MatKhau = '{mkMoi}' WHERE TaiKhoan LIKE '{username}'";

                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã đổi mật thành công", "Thông báo");
                    LoginInfo.MKLogin = mkMoi;
                }
                else
                {
                    MessageBox.Show("Đổi mật khẩu thất bại", "Thông báo");
                }
            }
            else
            {
                MessageBox.Show("Thông tin mật khẩu không trùng khớp", "Thông báo");
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            txtMatKhauCu.Clear();
            txtMatKhauMoi.Clear();
            txtXacNhan.Clear();
        }

        private void FrmDoiMatKhau_Load(object sender, EventArgs e)
        {
            txtTaiKhoan.Text = LoginInfo.TKLogin;
        }
    }
}
