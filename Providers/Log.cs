﻿using DevExpress.PivotGrid.OLAP;
using DevExpress.XtraEditors.Internal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhanMemQuanLy.Providers
{
    class Log
    {
        static string path = ConfigurationManager.AppSettings["ErrroLogPath"].ToString();

        public static void WriteErrorLog(string str)
        {
            FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(str);
            }
            fs.Flush();
            fs.Dispose();
            fs.Close();
        }
    }
}